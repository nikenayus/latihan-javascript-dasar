// membuat object
// object literal
  var mhs1 = {
    nama : "Ninda Ramadhanti",
    nrp : "033040007",
    email : "NindaRama@gmail.com",
    jurusan : "Busana Butik"
}
//Function Declaration
function buatObjectMahasiswa(nama, nrp, email, jurusan){
    var mhs = {}
    mhs.nama = nama;
    mhs.nrp = nrp;
    mhs.email = email;
    mhs.jurusan = jurusan;
    return mhs;
}

var mhs2 = buatObjectMahasiswa('Afnan Rifqi', '030450024', 'Afnanrif@gmail.com','Teknik Sepeda Motor')
// Constructor
function Mahasiswa(nama, nrp, email, jurusan){
    // var this = {};
    this.nama = nama;
    this.nrp = nrp;
    this.email = email;
    this.jurusan = jurusan;
    // retrun this;
}

var mhs3 = new Mahasiswa('Kansha Azzahra', '020134056', 'Kansha@yahoo.com', 'Rekayasa perangkat lunak');












